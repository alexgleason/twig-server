# Twig Server Package for Atom

Launches a small server from inside Atom to view your Twig templates.

![Screenshot 1](http://i.imgur.com/16KCXhW.png)
![Screenshot 2](http://i.imgur.com/WC9tyix.png)

# How to Install

```
apm install twig-server
```

Then restart Atom.

# How to Use

1. `File > Open Folder...`
2. Select your project folder
  1. The folder must contain an immediate subdirectory with your Twig templates
3. Open any file in that project
4. Use `Packages > Twig Server > Toggle` to turn on/off the server (or right click the editor and choose `Toggle Twig Server`)
5. Click the URL in the bottom panel to view
6. Configure your project, if necessary

# Configuration

1. `Edit > Open Your Config` to access your Atom configuration settings
2. Find the section called "twig-server" and change the projects within it according to your needs.

After changing any configuration settings, you must restart the server for changes to take effect.

## Settings

* **name** -- This is the identifier for your project. It's the name of your project's root folder.
* **views** -- The path for your templates folder relative to your project.
* **fileExtension** -- The file extension to use for your templates if none is specified in the URL. Ex: /index becomes /index.html if `html` is the setting here.
* **port** -- What port the server should run on for this project. Use `0` to indicate a random, unused port.
* **variables** -- An object containing variables you want parsed by Twig. For instance, `{ siteUrl: "example.com" }` will allow you to use `{{ siteUrl }}` in Twig.
