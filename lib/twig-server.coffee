TwigServerView = require './twig-server-view'
Server = require './server'
Config = require './config'
conf = null

{CompositeDisposable} = require 'atom'

module.exports = TwigServer =
  twigServerView: null
  modalPanel: null
  subscriptions: null

  config:
    projects:
      title: 'Projects'
      type: 'array'
      default: []
      items:
        title: 'Project'
        type: 'object'
        properties:
          name:
            title: 'Project name'
            type: 'string'
            default: 'default'
          views:
            title: 'Views directory'
            type: 'string'
            default: 'templates'
          fileExtension:
            title: 'Default file extension'
            type: 'string'
            default: 'html'
          port:
            title: 'Port number'
            type: 'integer'
            default: 0
            minimum: 0
          variables:
            title: 'Custom local variables'
            type: 'object'
            default: {}


  activate: (state) ->
    @twigServerView = new TwigServerView(state.twigServerViewState)
    @modalPanel = atom.workspace.addBottomPanel(item: @twigServerView.getElement(), visible: false)

    # Events subscribed to in atom's system can be easily cleaned up with a CompositeDisposable
    @subscriptions = new CompositeDisposable

    # Register command that toggles this view
    @subscriptions.add atom.commands.add 'atom-workspace', 'twig-server:toggle': => @toggle()
    @subscriptions.add atom.commands.add 'atom-workspace', 'twig-server:start-server': => @startServer()
    @subscriptions.add atom.commands.add 'atom-workspace', 'twig-server:stop-server': => @stopServer()

    @server = false

  deactivate: ->
    @modalPanel.destroy()
    @subscriptions.dispose()
    @twigServerView.destroy()

  serialize: ->
    twigServerViewState: @twigServerView.serialize()

  toggle: ->
    if @server is off
      @startServer()
    else
      @stopServer()

  startServer: ->
    @stopServer() if @server is on
    conf = new Config()
    @server = new Server()
    @server.start()
    @modalPanel.show()
    # Set the message
    host = @server.host()
    message = "Twig server running at <a href=\"http://#{host}\">#{host}</a> for project `<strong>#{conf.projectName}</strong>`"
    @twigServerView.setMessage(message)

  stopServer: ->
    conf = null
    @server.stop()
    @server = false
    @modalPanel.hide()
