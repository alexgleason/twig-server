module.exports =
class TwigServerView
  constructor: (serializedState) ->
    # Create root element
    @element = document.createElement('status-bar')
    @element.classList.add('status-bar')
    # Left side
    @left = document.createElement('div')
    @left.classList.add('status-bar-left')
    # Right side
    @right = document.createElement('div')
    @right.classList.add('status-bar-right')

    # Create message element
    @message = document.createElement('div')
    @message.classList.add('status-bar-server-message')
    @left.appendChild(@message)

    # Create close element
    @close = document.createElement('div')
    @close.classList.add('status-bar-server-close')
    @closeBtn = document.createElement('a')
    @closeBtn.classList.add('stop-server-button', 'icon-x')
    @closeBtn.textContent = 'Stop Server'
    @closeBtn.addEventListener 'click', =>
      atom.commands.dispatch atom.views.getView(atom.workspace), 'twig-server:stop-server'
      false
    @close.appendChild(@closeBtn)
    @right.appendChild(@close)

    # Add the elements
    @flexbox = document.createElement('div')
    @flexbox.classList.add('flexbox-repaint-hack')
    @flexbox.appendChild(@left)
    @flexbox.appendChild(@right)
    @element.appendChild(@flexbox)

  # Returns an object that can be retrieved when package is activated
  serialize: ->

  # Tear down any state and detach
  destroy: ->
    @element.remove()

  getElement: ->
    @element

  getMessageElement: ->
    @message

  setMessage: (message) ->
    @message.innerHTML = message
