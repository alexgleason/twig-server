path = require 'path'

class TwigServerProject
  constructor: (@name = 'default', @views = 'templates', @fileExtension = 'html', @port = 0, @variables = {}) ->

projectPath = ->
  activeFilePath = atom.workspace.getActivePaneItem().getPath()
  atom.project.relativizePath(activeFilePath)[0]
projectName = ->
  path.basename projectPath()

module.exports =
class Config
  constructor: ->
    @projectPath = projectPath()
    @projectName = projectName()

  activeProject: ->
    projects = atom.config.get('twig-server.projects')
    that = this # It's actually kind of upsetting how bad javascript is
    project = projects.filter (project) ->
      return project.name is that.projectName
    # If the project config wasn't found, create it
    if project.length is 0
      project.push new TwigServerProject(@projectName)
      project = project[0]
      projects.push project
      atom.config.set('twig-server.projects', projects)
    else
      project = project[0]
    project
