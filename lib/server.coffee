vm = require 'vm'
twig = require 'twig'
enableDestroy = require 'server-destroy'
path = require 'path'
Config = require './config'
conf = null

# FIXME: This hack allows eval() to run
# Without it, node module `depd` (in express.js) errors out
# Use something cleaner, like `atom/loophole` or a fork
global.eval = (source) -> vm.runInThisContext(source)

express = require 'express'

module.exports =
class Server
  constructor: ->
    # Get config settings
    conf = new Config()

    # Using express.js
    @app = express()
    # Set express.js configuration
    views = "#{conf.projectPath}/#{conf.activeProject().views}"
    @app.set 'views', views
    @app.set 'view engine', conf.activeProject().fileExtension
    @app.set 'view cache', false
    @app.engine 'html', twig.__express
    @app.engine 'twig', twig.__express
    @app.set 'twig options', {
        strict_variables: false
    }

    # Get the path in a better format, with fallbacks
    req_path = (req) ->
      path = req.path
      if path is '/' then path = '/index'
      path = path.substring(1, path.length)

    # Attempt to serve the request as a file
    process_file = (req, res, next) ->
      path = req_path(req)
      options =
        root: conf.projectPath
        dotfiles: 'deny'
        headers:
          'x-timestamp': Date.now()
          'x-sent': true

      res.sendFile path, options, (err) ->
        # If the file wasn't found, try the next method
        if err
          next()

    # Attempt to serve the request as a template
    process_template = (req, res) ->
      # Disable caching
      twig.cache false
      path = req_path(req)
      try
        res.render path, conf.activeProject().variables
      catch error
        res.send error

    # Route all traffic
    @app.get('*', process_file, process_template)

  start: ->
    # Start the server
    @server = @app.listen conf.activeProject().port
    enableDestroy @server

  stop: ->
    @server.destroy()

  host: ->
    host = @server.address().address
    port = @server.address().port
    if host is '' or host is '::' then host = 'localhost'
    host+':'+port

  # Returns an object that can be retrieved when package is activated
  serialize: ->

  # Tear down any state and detach
  destroy: ->
    @stop()
